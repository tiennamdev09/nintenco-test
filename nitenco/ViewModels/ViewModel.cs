﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace nitenco.ViewModels
{
    public class ViewModelIndex
    {
        public IEnumerable<SelectListItem> ProductList { get; set; }
        public IEnumerable<SelectListItem> CustomerList { get; set; }
        public IEnumerable<SelectListItem> CategoryList { get; set; }

        public ViewModelIndex()
        {
            ProductList = new List<SelectListItem>();
            CustomerList = new List<SelectListItem>();
            CategoryList = new List<SelectListItem>();
        }

    }

    public class ViewModel
    {
        public Guid ProductId { get; set;}

        public Guid CustomerId { get; set;}

        public Guid CategoryId { get; set;}

        public IEnumerable<SelectListItem> ProductList { get; set; }
        public IEnumerable<SelectListItem> CustomerList { get; set; }
        public IEnumerable<SelectListItem> CategoryList { get; set; }

        public ViewModel()
        {
            ProductList = new List<SelectListItem>();
            CustomerList = new List<SelectListItem>();
            CategoryList = new List<SelectListItem>();
        }

       
    }
}
