﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace nitenco.Models
{
    public class Order
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float Amount { get; set; }
        public DateTime OrderDate { get; set; }
        public Guid CustomerId { get; set; }
        [Required]
        [ForeignKey(nameof(CustomerId))]
        public virtual Customer Customer { get; set; }       
        public Guid ProductId { get; set; }
        [Required]
        [ForeignKey(nameof(ProductId))]
        public virtual Product Product { get; set; }



      /*  [NotMapped]
        public SelectList ProductList { get; set; }
        [NotMapped]
        public SelectList CustomerList { get; set; }

         public string? UserId { get; set; }
         [Required]
         [ForeignKey(nameof(UserId))]
         public virtual IdentityUser? User { get; set; } */
    }
}
