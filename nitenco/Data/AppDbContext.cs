﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace nitenco.Models.Database
{
    public class AppDbContext : IdentityDbContext
    {
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Category> Category { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                var tableName = entityType.GetTableName();
                if (tableName.StartsWith("AspNet"))
                {
                    entityType.SetTableName(tableName.Substring(6));
                }
            }


            modelBuilder.Entity<Customer>(a =>
            {
                a.HasData(new Customer
                {
                    Id = new Guid("90d10994-3bdd-4ca2-a178-6a35fd653c59"),
                    Username = "admin",
                    Address = "Hanoi",
                });

                a.HasData(new Customer
                {
                    Id = new Guid("90d10994-3bdd-4ca2-a178-6a35fd653c53"),
                    Username = "Nam",
                    Address = "Hanoi",
                });
            });

            modelBuilder.Entity<Category>(a =>
            {
                a.HasData(new Category
                {
                    Id = new Guid("90d10994-3bdd-4ca2-a178-6a35fd653c12"),
                    Description = "Ao",
                    
                });
                a.HasData(new Category
                {
                    Id = new Guid("90d10994-3bdd-4ca2-a178-6a35fd653c11"),
                    Description = "Quan",

                });
            });

            modelBuilder.Entity<Product>(a =>
            {
                a.HasData(new Product
                {
                    Id = new Guid("90d10994-3bdd-4ca2-a178-6a35fd123c59"),
                    Quantity = 1000,
                    Description = "MU",
                    Price = 100000,
                    CategoryId = new Guid("90d10994-3bdd-4ca2-a178-6a35fd653c12"),                  
                    
                });
                a.HasData(new Product
                {
                    Id = new Guid("90d10994-3bdd-4ca2-a178-6a35fd126c59"),
                    Quantity = 1000,
                    Description = "Quan MU",
                    Price = 100000,
                    CategoryId = new Guid("90d10994-3bdd-4ca2-a178-6a35fd653c12"),                  
                    
                });
            });
        }
    }
}
