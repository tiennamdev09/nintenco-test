﻿using nitenco.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace nitenco.Request
{
    public class OrderRequest
    {
        public string Name { get; set; }
        public float Amount { get; set; }
        public DateTime OrderDate { get; set; }
        public Guid CustomerId { get; set; }       
        public Guid ProductId { get; set; }
     
    }
}
