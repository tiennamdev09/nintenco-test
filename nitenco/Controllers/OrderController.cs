﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nitenco.Models;
using nitenco.Models.Database;
using nitenco.Request;

namespace nitenco.Controllers
{
    public class OrderController : Controller
    {
        private readonly AppDbContext _context;
        public OrderController(AppDbContext appDbContext)
        {
            _context = appDbContext;
        }


       // GET: Orders
        public async Task<IActionResult> Index()
        {
            return _context.Order != null ?
                        View(await _context.Order.Include(x=>x.Product).Include(x => x.Product.Category).Include(x => x.Customer).ToListAsync()):
                        Problem("Entity set 'ApplicationDbContext.Product'  is null.");
        }

        // GET: Order/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Amount,Name,OrderDate,CustomerId,ProductId")] OrderRequest orderRequest)
        {
          var product = await _context.Product.FindAsync(orderRequest.ProductId);
        if(orderRequest.Amount > product.Quantity)
            {
                ViewBag.Error = "amount of the order is greater than the \r\nquantity of the product";
                return View();
            }
        else
        {         
            if (ModelState.IsValid)
            {
            var newOrder = new Order()
            {
                Id = new Guid(),
                Name = orderRequest.Name,
                Amount = orderRequest.Amount,
                OrderDate = orderRequest.OrderDate,
                CustomerId = orderRequest.CustomerId,
                ProductId  = orderRequest.ProductId,              
            };
            _context.Add(newOrder);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
            }                     
                }
            return View();
        }
    }
}
